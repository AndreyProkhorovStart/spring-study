package com.train.services;

import com.train.exceptions.BookNotFoundException;
import com.train.exceptions.NoDataFoundException;
import com.train.repositories.BookRepository;
import org.jooq.tools.json.JSONArray;
import org.jooq.tools.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tej.JooQDemo.jooq.sample.model.tables.pojos.Book;

import java.util.List;

/**
 * Компонент-фасад для бизнесло логики содержащий работу
 * с репозиториями.
 * <p>
 * Посмотреть паттерн фасад.
 */
@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public List<Book> getAll() {
        List<Book> books = bookRepository.getAll();
        if (books.isEmpty()) {
            throw new NoDataFoundException();
        }
        return books;
    }


    public Integer create(Book book) {
        return bookRepository.insert(book);
    }

    /**
     * Заглушка чтобы понимать, какие данные нужны для отправки.
     *
     * @return
     */
    public JSONArray getCreateForm() {
        JSONArray response = new JSONArray();
        JSONObject json = new JSONObject();
        // Формируем JSON
        json.put("title", "Для наименования книги");
        json.put("author", "Для наименования автора");
        json.put("pages", "Количество страниц");
        json.put("cost", "Стоимость книги");
        response.add(json);
        return response;
    }

    public Book read(Integer id) {
        // Получаем экземпляр модели
        Book book = bookRepository.getById(id);
        if(book == null) throw new BookNotFoundException(id);
        return book;
    }

    /**
     * При обновлении также проверяем наличии записи.
     *
     * @param id
     * @param book
     */
    public void update(Integer id, Book book) {
        if (bookRepository.isExist(id)) {
            bookRepository.update(id, book);
        } else {
            throw new BookNotFoundException(id);
        }
    }

    /**
     *
     * @param id
     */
    public void delete(Integer id) {
        if (bookRepository.isExist(id)) {
            bookRepository.delete(id);
        } else {
            throw new BookNotFoundException(id);
        }
    }
}
