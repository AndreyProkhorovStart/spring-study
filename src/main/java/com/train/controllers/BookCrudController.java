package com.train.controllers;

/**
 * @Author : Andrey
 * @ Описание: предоставалять доступ к книгам
 * @Date : Created in 18:28 2018/4/12
 * @Modified By : Andrey
 */
import com.train.dto.BookDto;
import com.train.dto.converters.BookConverter;
import com.train.services.BookService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
public class BookCrudController {

    @Autowired
    private BookConverter bookConverter;

    @Autowired
    private BookService bookService;

    /**
     * Выборка всех записей
     *
     * @return
     */
    @ApiOperation(value = "Получить сведения о книгах", notes = "Получить сведения о книгах")
    @GetMapping(value = "/books")
    public ResponseEntity<Object> index() {

        // Также дополнительно сформируем ответ
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("data", bookService.getAll());

        return new ResponseEntity<>(body,HttpStatus.OK);

    }

    /**
     * Получение формы для создания записи
     *
     * @return
     */
    @GetMapping(value = "/books/create")
    public ResponseEntity<Object> create() {

        // Также дополнительно сформируем ответ
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("data", bookService.getCreateForm());

        return new ResponseEntity<>(body, HttpStatus.OK);

    }

    /**
     * Создание записи
     * @Valid - сообщить спрингу передать объект Валидатору, прежде чем делать с ним что-либо еще
     * ResponseEntity - Это специальный класс, который представляет
     *  http-ответ. Он содержит тело ответа, код состояния,
     *  заголовки. Мы можем использовать его для более тонкой настройки http-ответа.
     *
     * @param request
     * @return
     */
    @PostMapping(value = "/books", consumes = "application/json")
    public ResponseEntity<Object> store(@RequestBody @Valid BookDto request) {
        //Видоизменили запись, добавив конвертацию в pojo Book
        int id = bookService.create(
                bookConverter.toModel(request)
        );
        // Также дополнительно сформируем ответ
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", "Создана книга с идентификатором " + id);

        return new ResponseEntity<>(body, HttpStatus.OK);
    }

    /**
     * Получение конкретной записи
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/books/{id}")
    public ResponseEntity<Object> show(@PathVariable(value = "id", required = true) Integer id) {

        // Также дополнительно сформируем ответ
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put(
                "data",
                bookConverter.toDto(bookService.read(id))
        );

        return new ResponseEntity<>(body, HttpStatus.OK);

    }

    /**
     * Получение формы для редактирования записи (связка с update)
     *
     * Разница между show и edit, заключена в том, что могут понадобиться дополнительные
     * данные, которые неотображются в простом просмотре записи.
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/books/{id}/edit")
    public ResponseEntity<Object> edit(@PathVariable(value = "id", required = true) Integer id) {

        // Также дополнительно сформируем ответ
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put(
                "data",
                bookConverter.toDto(bookService.read(id))
        );

        return new ResponseEntity<>(body, HttpStatus.OK);

    }

    /**
     * Обновление записи
     *
     * @param id
     */
    @PutMapping(value = "/books/{id}", consumes = "application/json")
    public ResponseEntity<Object> update(@PathVariable(value = "id", required = true) Integer id, @RequestBody @Valid BookDto request) {

        bookService.update(
                id,
                bookConverter.toModel(request)
        );

        // Также дополнительно сформируем ответ
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", "Обновлена книга с идентификатором " + id);

        return new ResponseEntity<>(body,HttpStatus.OK);
    }

    /**
     * Удаление записи
     *
     * @param id
     */
    @DeleteMapping("/books/{id}")
    public ResponseEntity<Object> JSONArray(@PathVariable(value = "id", required = true) Integer id) {

        bookService.delete(id);

        // Также дополнительно сформируем ответ
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", "Удалена книга с идентификатором " + id);

        return new ResponseEntity<>(body,HttpStatus.OK);
    }
}

