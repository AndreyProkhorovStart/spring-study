package com.train.merged_pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserWithCity {
    private Integer id;
    private String name;
    private Integer idCity;
    private String cityName;
}
